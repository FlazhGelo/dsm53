<?php

use Illuminate\Database\Seeder;

class DireccionesSeeder extends Seeder{
    public function run(){
      DB::table('direcciones')->insert([
          'id' => '2',
          'direccion' => 'Tecnologias de la informacion y comunicacion',
          'abreviatura' => 'redes',
          'estatus' => 'activo',]);
    }
}
