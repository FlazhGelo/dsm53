<?php

use Illuminate\Database\Seeder;

class CarrerasSeeder extends Seeder{
    public function run(){
      DB::table('carreras')->insert([
          'id' => '1',
          'direccion_id' => '2',
          'carrera' => 'Desarrollo de Software Multiplataformas',
          'abreviatura' => 'DSM',
          'estatus' => 'activo',]);
    }
}
