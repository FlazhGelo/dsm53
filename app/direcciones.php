<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class direcciones extends Model{
    protected $table='direcciones';
    protected $fillable=['direccion','abreviatura','estatus'];
    public function carreras(){
      return $this->belongsTo('App\carreras','direccion_id','id');
    }
}
