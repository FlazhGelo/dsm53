<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carreras extends Model{
  protected $table='carreras';
  protected $fillable=['direccion_id','carrera','abreviatura','estatus'];
  public function direcciones(){
    return $this->belongsTo('App\direcciones','direccion_id','id');
  }
  public function asignaturas(){
    return $this->belongsTo('App\asignaturas','carrera_id','id');
  }
}
