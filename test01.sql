CREATE TABLE `alumnos` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255),
  `carrera` varchar(255),
  `grupo` varchar(255),
  `matricula` int
);

CREATE TABLE `docentes` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255),
  `matricula` int,
  `carrera` varchar(255),
  `practica_id` int,
  `alumnos_id` int
);

CREATE TABLE `practica` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `materiales` varchar(255),
  `cantidad` int,
  `fecha` int,
  `carrera` varchar(255),
  `laboratorio` int,
  `hora` timestamp
);

ALTER TABLE `docentes` ADD FOREIGN KEY (`practica_id`) REFERENCES `practica` (`id`);

ALTER TABLE `docentes` ADD FOREIGN KEY (`alumnos_id`) REFERENCES `alumnos` (`id`);
